//Factorial.java
import java.util.Scanner;
//Here is Factorial Class 
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 //if integer is negative, output conflict
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
//else do a forloop to calculate factorial of input number
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
